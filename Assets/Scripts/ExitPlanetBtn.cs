﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitPlanetBtn : MonoBehaviour
{
    [SerializeField]
    private GameData gameData;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Clk);
    }

    // Update is called once per frame
    void Update()
    {
        ShowHide();
    }

    void Clk()
    {
        gameData.OnPlanet = false;
    }
    void ShowHide()
    {
        if(gameData.OnPlanet == true)
        {
            transform.localPosition = new Vector3(0,0,0);
        }
        else
        {
            transform.localPosition = new Vector3(1000, 0, 0);
        }
    }
}
