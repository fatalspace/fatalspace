﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFleet : MonoBehaviour
{
    public GameObject _enemy;
    public float enemyBornTime = 1.5f;
    float enemyBornDeltaTime;

    [SerializeField]
    private GameData gameData;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        CheckSelfInst();
    }
    // Update is called once per frame
    void Update()
    {
        EnemyBorn();
    }

    void EnemyBorn()
    {
        if (enemyBornDeltaTime > enemyBornTime)
        {
            GameObject enemy = Instantiate(_enemy, transform);
            enemy.transform.localPosition = new Vector3(Random.Range(-2f, 2f), 3f, 0f);
            enemyBornDeltaTime = 0;
        }
        else
        {
            enemyBornDeltaTime += Time.deltaTime;
        }

    }

    void CheckSelfInst()
    {
        GameObject[] a = GameObject.FindGameObjectsWithTag("EnemyFleet");
            
        if (a.Length >1)
        {
            Destroy(gameObject);
        }
    }
}