﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlanetManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    [SerializeField] List<Planet> planetList;

    public Planet CreatePlanet(int typePlanet)
    {
        Planet planet = Instantiate(planetList[typePlanet]);
        return planet;
    }
}
