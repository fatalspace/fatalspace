﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapScrollbar : MonoBehaviour
{
    [SerializeField]
    private GameData gameData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Scrollbar>().value = (gameData.PlrCurrentPosOnLvl/1000)*-1;
    }
}
