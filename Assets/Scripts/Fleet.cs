﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fleet : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    [SerializeField] bool move = false;
    [SerializeField] float fleetMoveSpeed = 1.5f;

    #region IsPlrFleet
    public bool plrFleet = false;
    public void IsPlrFleet(bool setPlrFleet)
    {
        plrFleet = setPlrFleet;
        SetMove();
    }
    #endregion

    #region Slot
    [SerializeField] List<List<int>> slotMask;
    [SerializeField] List<List<List<int>>> slotMaskList = new List<List<List<int>>>
    {
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {2,2,3,2,2},//moveable = false,size = 2 ,size = 2,size = 3 ...
                    new List<int> {1,2,2,2,1},
                    new List<int> {1,1,1,1,1}
            },//moveable = true,size = 2 ,size = 2,size = 3 ...
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {1,1,1,1,1},//moveable = false,size = 2 ,size = 2,size = 3 ...
                    new List<int> {1,1,1,1,1},
                    new List<int> {1,1,1,1,1}
            },
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {3,3,3,3,3},//moveable = false,size = 2 ,size = 2,size = 3 ... 
                    new List<int> {3,3,3,3,3},
                    new List<int> {3,3,3,3,3}
            },
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {1,1,3,1,1}//moveable = false,size = 2 ,size = 2,size = 3 ... 
            }
    };
    public void SetSlotMask(int idFleetSlotMask)
    {
        if (slotMaskList[idFleetSlotMask] != null)
        {
            slotMask = slotMaskList[idFleetSlotMask];
            CreateSlot();
        }
        else
        {
            Debug.Log("No SlotMas ID:" + idFleetSlotMask, gameObject);
        }
    }
    public List<List<int>> GetSlotMask()
    {
        return slotMask;
    }

    public Slot _slot;    
    void CreateSlot()
    {
        for (int i = 0; i < slotMask.Count; i++)
        {
            for (int a = 0; a < slotMask[i].Count; a++)
            {                
                Slot slot = Instantiate(_slot, transform);
                slotList.Add(slot);
                slot.SetSize(slotMask[i][a]);
                slot.SetPos(i, a);                
            }
        }
    }

    [SerializeField] List<Slot> slotList;   
    public Slot GetSlot(int row, int colum)
    {        
        for (int i = 0; i < slotList.Count; i++)
        {
            Slot slot = slotList[i];

            if(slot.row == row && slot.column == colum)
            {
                return slot;
            }
        }
        Debug.Log("No slot in row:" + row + " colum:" + colum);
        return null;
    }
    #endregion

    [SerializeField] List<Ship> shipInFleetList = new List<Ship>();
    public void PutShipInSlot(Ship ship, Slot slot)
    {
        slot.AddShip(ship);
        shipInFleetList.Add(ship);
        ship.fleet = this;
    }

    private void Start()
    {        
        SetPosition();        
    }
    private void Update()
    {
        Move();
        StopFleetMove();
        CheckPosToDestroy();
        CheckShipInFleet();
    }

    #region MOVE
    void SetMove()
    {
        if(plrFleet == false)
        {
            move = true;
        }
    }
    void Move()
    {
        if (move == true)
        {
            transform.Translate(Vector3.up * Time.deltaTime * fleetMoveSpeed);
        }
    }    
    void StopFleetMove()
    {
        if (transform.position.y < 4.5f)
        {            
            move = false;
        }
    }
    #endregion

    void SetPosition()
    {
        if (plrFleet == true)
        {
            transform.position = new Vector3(-2, -3.5f, 0);
        }
        else
        {
            transform.Rotate(0, 0, 180);
            transform.position = new Vector3(2, 10f, 0);
        }
    }
    void CheckPosToDestroy()
    {
        if (transform.position.y < -5)
        {
            gameManagers.RemoveFleet(this);
        }
    }

    public void ShipKill(Ship ship)
    {
        shipInFleetList.Remove(ship);
    }
    void CheckShipInFleet()
    {
        if(shipInFleetList.Count == 0)
        {
            gameManagers.RemoveFleet(this);
        }        
    }
}
