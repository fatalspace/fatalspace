﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleetManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    public delegate void StopShootDel();
    public static event StopShootDel StopShootEvent;

    public Fleet _fleet;
    [SerializeField] List<Fleet> fleetList;

    public void CreateFleet(bool plrFleet, int idFleetSlotMask, int idShipMask)
    {
        Fleet fleet = Instantiate(_fleet);
        fleetList.Add(fleet);
        
        fleet.IsPlrFleet(plrFleet);
        fleet.SetSlotMask(idFleetSlotMask);

        gameManagers.PutShipInFleet(fleet, idShipMask);       
    }
    public void RemoveFleet(Fleet fleet)
    {
        if(fleetList.Contains(fleet))
        {
            if(StopShootEvent != null)
            {
                StopShootEvent();
            }

            fleetList.Remove(fleet);            
            Debug.Log("Remove fleet:" + fleet);
            Destroy(fleet.gameObject);
        }
    }

    public void ShipKill(Ship ship, Fleet fleet)
    {
        Fleet f = fleetList[fleetList.IndexOf(fleet)];
        f.ShipKill(ship);
    }
}
