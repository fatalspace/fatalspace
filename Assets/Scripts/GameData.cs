﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameData", menuName = "GameData", order = 51)]
public class GameData : ScriptableObject
{
    [SerializeField]
    private bool pauseGame = false;
    [SerializeField]
    private bool gameOnLine = false;
    [SerializeField]
    private int money = 0;
    [SerializeField]
    private float plrCurrentPosOnLvl = 0;
    [SerializeField]
    private bool onPlanet = false;
    [SerializeField]
    private GameObject currentPlanet;
    [SerializeField]
    private List<Planet> planetList;
    [SerializeField]
    private Fleet fleet;

    public bool PauseGame
    {
        get
        {
            return pauseGame;
        }
        set
        {
            pauseGame = value;
        }
    }

    public bool GameOnLine
    {
        get
        {
            return gameOnLine;
        }
        set
        {
            gameOnLine = value;
        }
    }

    public int Money
    {
        get
        {
            return money;
        }
        set
        {
            money += value;
        }
    }

    public float PlrCurrentPosOnLvl
    {
        get
        {
            return plrCurrentPosOnLvl;
        }
        set
        {
            plrCurrentPosOnLvl = value;
        }
    }

    public bool OnPlanet
    {
        get
        {
            return onPlanet;
        }
        set
        {
            onPlanet = value;
        }
    }

    public GameObject CurrentPlanet
    {
        get
        {
            return currentPlanet;
        }
        set
        {
            currentPlanet = value;
        }
    }

    public List<Planet> PlanetList
    {
        get
        {
            return planetList;
        }
        set
        {
            planetList = value;
        }
    }

   public Fleet Fleet
    {
        get
        {
            return fleet;
        }
        set
        {
            fleet = value;
        }
    }


    public GameData()
    {        
        this.PlanetList = new List<Planet>();
    }
}
