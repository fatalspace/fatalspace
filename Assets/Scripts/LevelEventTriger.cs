﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelEventTriger : MonoBehaviour
{
    public delegate void LevelEventTrigerDelegate(LevelEventTriger levelEventTriger);
    public static event LevelEventTrigerDelegate LevelEvent;

    public int type;
    public int idFleetSlotMask;
    public int IdShipMask;
    public int planetType;

    
    public void SetEventType(int setType)
    {
        type = setType;
    }
    public void SetPlanetType(int setPlanetType)
    {
        planetType = setPlanetType;
    }
    public void SetIdFleetSlotMask(int setIdFleetSlotMask)
    {
        idFleetSlotMask = setIdFleetSlotMask;
    }
    public void SetIdShipMask(int setIdShipMask)
    {
        IdShipMask = setIdShipMask;
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Fleet")
        {
            if (LevelEvent != null)
            {
                LevelEvent(this);
            }
        }
    } 
}
