﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleetMap : MonoBehaviour
{
    public List<List<int>> fleetMapSlotSize;    

    List<List<int>> startFleetMap = new List<List<int>>
        {//карта флота, матрица слотов int = slotSize
                new List<int> {0,2,2,3,2,2 },//moveable = false,size = 2 ,size = 2,size = 3 ...
                new List<int> {0,1,2,2,2,1 },
                new List<int> {1,1,1,1,1,1}//moveable = true,size = 2 ,size = 2,size = 3 ...
        };

    List<List<int>> npcFleet1 = new List<List<int>>
    {
         new List<int> {1,1,1,1,1,1 }
    };

    List<List<int>> npcFleet2 = new List<List<int>>
    {
        new List<int> {0,1,1,2,1,1 },
        new List<int> {1,1,1,1,1,1 }
    };

    public void CreateFleetMap(int idFleetMap)
    {
        if(idFleetMap == 0)
        {
            //fleetMap = startFleetMap;
        }
        else if(idFleetMap == 1)
        {
           // fleetMap = npcFleet1;
        }
        else if (idFleetMap == 2)
        {
           // fleetMap = npcFleet2;
        }
        else
        {
            Debug.Log("<color=red>Fleet no ID:</color>"+idFleetMap,gameObject);            
        }
    }
}
