﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //--------FleetFirstLineMove Start
    Vector3 rightDestinationPoint;
    Vector3 leftDestinationPoint;    
    float time;
    bool reverseMode = false;
    public bool moveEnable = true;
    //--------FleetFirstLineMove END
    [SerializeField]
    private GameData gameData;
    int price;


    // Start is called before the first frame update
    void Start()
    {
        //gameData = GameObject.Find("GameData").GetComponent<GameData>();////////////////!!!!!!!!!!!!!!!!!

        rightDestinationPoint = new Vector3(1.5f, transform.position.y, transform.position.z);
        leftDestinationPoint = new Vector3(-1.5f, transform.position.y, transform.position.z);
        RandomReverseMode();
        RandomPrice();
    }

    // Update is called once per frame
    void Update()
    {
        
        Move();
    }

    void Move()
    {
        if (moveEnable == true)
        {
            if (reverseMode == false)
            {
                transform.position = Vector3.Lerp(leftDestinationPoint, rightDestinationPoint, time);
            }
            else
            {
                transform.position = Vector3.Lerp(rightDestinationPoint, leftDestinationPoint, time);
            }

            time += Time.deltaTime;

            if (time > 1)
            {
                time = 0;

                if (reverseMode == true)
                {
                    reverseMode = false;
                }
                else
                {
                    reverseMode = true;
                }
            }
        }
        else
        {
            transform.position = new Vector3(0f, transform.position.y, transform.position.z);
        }
    }

    void RandomReverseMode()
    {
        int i = Random.Range(0, 1);
        if(i == 0)
        {
            reverseMode = false;
        }
        else
        {
            reverseMode = true;
        }

    }

    void RandomPrice()
    {
        price = Random.Range(0, 5);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Bullet")
        {
            Destroy(other.gameObject);
            Death();
        }
    }

    void Death()
    { 
        gameData.Money = price;
        Destroy(gameObject);
    }
}
