﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour
{
    [SerializeField]int size;
    public void SetSize(int setSize)
    {
        size = setSize;
        SetName(size);
    }

    public int row;
    public int column;
    public void SetPos(int setRow, int setColumn)
    {
        row = setRow;
        column = setColumn;

        transform.localPosition = new Vector3(setColumn, setRow, 0);
    }

    Ship ship;
    public void AddShip(Ship setShip)
    {
        if(ship != null)
        {
            Debug.Log("Slot already have ship.", gameObject);
        }
        else
        {
            if(size < setShip.size)
            {
                Debug.Log("Slot to small", gameObject);
            }
            else
            {                
                ship = setShip;
                ship.transform.SetParent(transform);
                ship.transform.localPosition = Vector3.zero;
            }
            
        }
    }    

    void SetName(int size)
    {
        name = "Slos S:" + size;
    }
}
