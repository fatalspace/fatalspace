﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetInfo : MonoBehaviour
{
    public Text _planetName;
    public Image _img;
    public Button _factoryBtn;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetPlanetName(string planetName)
    {
        _planetName.text = planetName;
    }

}
