﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField] GameManagers gameManagers;   

    private void Start()
    {        
        InitGame(); 
    }

    void InitGame()
    {
        InitGameManagers();

        StartApp();
    }

    void InitGameManagers()
    {
        gameManagers = Instantiate(gameManagers, transform);
        gameManagers.InitAllManagers();
    }
    void StartApp()
    {
        gameManagers.StartApp();
    }































    //public Fleet _fleet;
    //public FleetMapManager _fleetMapManager;
    //public ShipManager _shipManager;
    //public Level _level;

    //void Start()
    //{
    //    StartNewGame();    
    //}

    //void StartNewGame()
    //{
    //    Fleet fleet = Instantiate(_fleet);        
    //    ShipManager shipManager = Instantiate(_shipManager);

    //    fleet.SetShipManager(shipManager);

    //    fleet.CreateFleet(true, 0, 0);//создаем флот игрока.        

    //    Level level = Instantiate(_level);
    //    level.LoadLevel(1);
    //}


    //    AsyncOperation ao;
    //    
    //    // Start is called before the first frame update
    //    void Start()
    //    {
    //        //SceneManager.LoadScene(0, LoadSceneMode.Additive);
    //        ao = SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);
    //        ao.allowSceneActivation = false;        
    //
    //
    //        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    //        SceneManager.LoadScene(2, LoadSceneMode.Additive);
    //        SceneManager.LoadScene(3, LoadSceneMode.Additive);
    //
    //       // SceneManager.UnloadSceneAsync(4);
    //
    //       // SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(0));
    //
    //
    //       // AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Scene3");
    //    }
    //
    //    // Update is called once per frame
    //    void Update()
    //    {
    //        LoadScene();        
    //    }
    //
    //    void LoadScene()
    //    {
    //        if (!ao.isDone)
    //        {
    //            Debug.Log("Loading scene " + " [][] Progress: " + ao.progress);
    //        }
    //        else
    //        {
    //            ao.allowSceneActivation = true;
    //            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(0));
    //            SceneManager.UnloadSceneAsync(4);
    //        }
    //    }

}
