﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{    
    [SerializeField] LevelEventTriger _levelEventTriger;    
    List<LevelEventTriger> levelEventTrigerList = new List<LevelEventTriger>();

    [SerializeField] ParticleSystem particleSystemStar;
    [SerializeField] ParticleSystem particleSystemTumanosti;

    [SerializeField] bool move = false;    
    [SerializeField] float lvlFallSpeed = 1.5f;
    [SerializeField] int disBetweenEvent = 15;
    [SerializeField] List<List<int>> _levelMap;

    #region levelMapList
    List<List<List<int>>> levelMapList = new List<List<List<int>>>
    {
       new List<List<int>>() //карта уровня
            {
                new List<int>(){0,1},//0 - планета, 1 - тип планеты
                new List<int>(){1,2,1},//1 - флот, 1 - idFleetSlotMask, 1 - idShipMask
                new List<int>(){1,2,2},//1 - флот, 1 - idFleetSlotMask, 1 - idShipMask
                new List<int>(){1,2,3},//1 - флот, 1 - idFleetSlotMask, 1 - idShipMask
                new List<int>(){0,2}//0 - планета, 1 - тип планеты
            }
    };
    #endregion

    public void LoadLevel(int levelNumb)
    {
        if(levelMapList[levelNumb] != null)
        {           
            _levelMap = levelMapList[levelNumb];
            CreateLevelTriger();
            move = true;
        }
        else
        {
            Debug.Log("No level ID:" + levelNumb, gameObject);
        }
    }

    void CreateLevelTriger()
    {
        LevelEventTriger levelEventTriger = null;

        for (int i = 0; i < _levelMap.Count; i++)
        {            
            if(_levelMap[i][0] == 0)
            {
                levelEventTriger = CreatePlanetLevelTriger(_levelMap[i][1]);
            }
            else if(_levelMap[i][0] == 1)
            {
                levelEventTriger = CreateFleetLevelTriger(_levelMap[i][1], _levelMap[i][2]);
            }

            if(levelEventTriger != null)
            {
                levelEventTrigerList.Add(levelEventTriger);
                levelEventTriger.transform.SetParent(transform);
                levelEventTriger.transform.localPosition = new Vector3(0, i * disBetweenEvent + 10, 0);
            }            
        }
    }
    LevelEventTriger CreatePlanetLevelTriger(int planetType)
    {
        LevelEventTriger levelEventTriger = Instantiate(_levelEventTriger, transform);
        levelEventTriger.SetEventType(0);//id=0 planet game event
        levelEventTriger.SetPlanetType(planetType);
        return levelEventTriger;
    }
    LevelEventTriger CreateFleetLevelTriger(int idFleetSlotMask, int idShipMask)
    {
        LevelEventTriger levelEventTriger = Instantiate(_levelEventTriger, transform);
        levelEventTriger.SetEventType(1);//id=1 fleet game event
        levelEventTriger.SetIdFleetSlotMask(idFleetSlotMask);
        levelEventTriger.SetIdShipMask(idShipMask);
        return levelEventTriger;
    }

    public void PutPlanetInLevel(Planet planet)
    {
        planet.transform.SetParent(transform);
    }
    private void Update()
    {
        Move();        
    }

    void Move()
    {
        if (move == true)
        {
            for (int i = 0; i < levelEventTrigerList.Count; i++)
            {
                levelEventTrigerList[i].transform.Translate(Vector3.down * Time.deltaTime * lvlFallSpeed);
            }            
        }
    }
    public void MoveEnable()
    {
        move = true;
        ParticleSystemPlay();
    }
    void ParticleSystemPlay()
    {
        particleSystemStar.Play();
        particleSystemTumanosti.Play();
    }
    public void MoveDisable()
    {
        move = false;
        ParticleSystemPause();        
    }
    void ParticleSystemPause()
    {
        particleSystemStar.Pause();
        particleSystemTumanosti.Pause();
    }
}
