﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion
    
    [SerializeField] StartScreenPanel startScreenPanel;

    private void Start()
    {
        InitUI();
    }
    void InitUI()
    {
        startScreenPanel = Instantiate(startScreenPanel, transform);
    }


    public void StartApp()
    {
        startScreenPanel.Show();
    }

    public void HideAll()
    {
        GamePanel[] allGamePanel = GetComponentsInChildren<GamePanel>();
        foreach(var gamePanel in allGamePanel)
        {
            gamePanel.Hide();
        }
    }

}
