﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleetMapManager : MonoBehaviour
{
    List<List<int>> fleetMapSlot;

    List<List<int>> startFleetMap = new List<List<int>>
        {//карта флота, матрица слотов int = slotSize
                new List<int> {0,2,2,3,2,2 },//moveable = false,size = 2 ,size = 2,size = 3 ...
                new List<int> {0,1,2,2,2,1 },
                new List<int> {1,1,1,1,1,1}//moveable = true,size = 2 ,size = 2,size = 3 ...
        };

    List<List<int>> npcFleet1 = new List<List<int>>
    {
         new List<int> {1,1,1,1,1,1 }
    };

    List<List<int>> npcFleet2 = new List<List<int>>
    {
        new List<int> {0,1,1,2,1,1 },
        new List<int> {1,1,1,1,1,1 }
    };

    public List<List<int>> ReturnFleetMap(int idFleetMapSlot)
    {
        if (idFleetMapSlot == 0)
        {
            fleetMapSlot = startFleetMap;
            return fleetMapSlot;
        }
        else if (idFleetMapSlot == 1)
        {
            fleetMapSlot = npcFleet1;
            return fleetMapSlot;
        }
        else if (idFleetMapSlot == 2)
        {
            fleetMapSlot = npcFleet2;
            return fleetMapSlot;
        }
        else
        {
            Debug.Log("<color=red>FleetMapSlot no ID:</color>" + idFleetMapSlot, gameObject);
            return null;
        }
    }
}
