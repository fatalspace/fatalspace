﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameFieldBtn : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(Click);        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Click()
    {
        SceneManager.LoadScene(0);
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("GameFieldScene"));
    }
}
