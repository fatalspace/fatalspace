﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipSlot : MonoBehaviour
{
    [SerializeField] int size;
    [SerializeField] Ship ship;

    public void SetSize(int slotSize)
    {        
        size = slotSize;
        name = "Slot: S" + size;
        transform.localScale = new Vector3(size, size,0);
    }
    public void SetPosition(int x, int y, int z)
    {
        transform.localPosition = new Vector3(x, y, z);
    }
    public void AddShip(Ship setShip)
    {
        if (setShip.size <= size)
        {
            ship = setShip;
            ship.transform.SetParent(transform);
            ship.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("<color=red>Ship size:"+setShip.size+" Slot size:"+size+"</color>");
        }
    }
}
