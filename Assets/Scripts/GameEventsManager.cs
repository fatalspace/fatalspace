﻿using UnityEngine;
using System.Collections;

public class GameEventsManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    public delegate void ShipShootDel();
    public static event ShipShootDel ShipShootEvent;

    public delegate void ShipShootStartDel();
    public static event ShipShootStartDel ShipShootStartEvent;

    public delegate void ShipShootStoptDel();
    public static event ShipShootStoptDel ShipShootStopEvent;
   
    private void Start()
    {
        LevelEventTriger.LevelEvent += LevelEventManager;
        FleetManager.StopShootEvent += ShipShootStop;
        Ship.ShipKillEvent += ShipKill;        
    }

    void LevelEventManager(LevelEventTriger levelEventTriger)
    {
        if (levelEventTriger.type == 0)//planet
        {
            gameManagers.CreatePlanet(levelEventTriger.planetType);
            gameManagers.LevelEventTrigerStop();
        }
        else if (levelEventTriger.type == 1)//fleet
        {
            gameManagers.CreateFleet(false, levelEventTriger.idFleetSlotMask, levelEventTriger.IdShipMask);
            ShipShootStart();
            gameManagers.LevelEventTrigerStop();
        }
        else
        {
            Debug.Log("No type levelEventTriger:" + levelEventTriger.type, gameObject);
        }
    }

    void ShipShootStop()
    {
        if (ShipShootStopEvent != null)
        {
            ShipShootStopEvent();
        }
    }
    void ShipShootStart()
    {
        if (ShipShootStartEvent != null)
        {
            ShipShootStartEvent();
        }
    }

    void ShipKill(Ship ship, Fleet fleet)
    {
        gameManagers.ShipKill(ship, fleet);
    }  
}
