﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public int size = 0;
    public int type;
    [SerializeField] bool shoot = false;
    public bool plr = false;
    public int hp = 10;

    public Bullet _bullet;
    public float shootTime = 0.35f;
    float shootTimeDelta;

    public GameObject exposion;   

    public Fleet fleet;

    public delegate void ShipKillDel(Ship ship, Fleet fleet);
    public static event ShipKillDel ShipKillEvent;

    private void Awake()
    {        
        GameEventsManager.ShipShootStartEvent += ShootEnable;
        GameEventsManager.ShipShootStopEvent += ShootDisable;
    }

    private void Start()
    {        
        SetShootTime();        
    }

    void Update()
    {
       Shoot();
    }

    #region Shoot
    void SetShootTime()
    {
        if(type == 0)
        {
            shootTime = 2f;
        }
        else if(type == 1)
        {
            shootTime = 1.5f;
        }
        else if (type == 2)
        {
            shootTime = 1f;
        }
        else if (type == 3)
        {
            shootTime = 0.5f;
        }
        else if (type == 4)
        {
            shootTime = 1f;
        }
        else if (type == 5)
        {
            shootTime = 1.5f;
        }
        else
        {
            Debug.Log("ShootTime not set", gameObject);
        }        
    }
    void Shoot()
    {
        if (shoot == true)
        {
            if (shootTimeDelta > shootTime)
            {
                Bullet bullet = Instantiate(_bullet);
                bullet.transform.SetParent(transform);
                bullet.SetOwner(this);

                if (type == 1)
                {
                    bullet.speed = 0;
                }

                bullet.transform.position = new Vector3(transform.position.x, transform.position.y, _bullet.transform.position.z);
                shootTimeDelta = 0;
            }
            else
            {
                shootTimeDelta += Time.deltaTime;
            }
        }
    }
    void ShootEnable()
    {
        shoot = true;       
    }
    void ShootDisable()
    {
        shoot = false;
    }
#endregion

    public void SetPlr(bool setPlr)
    {
        plr = setPlr;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {     
        if (collision.tag == "Bullet")
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();

            if (bullet.ship.plr != plr)
            {
                Hit(bullet);
                if (bullet.name != "Lazer_Ray(Clone)")
                {
                    Destroy(bullet.gameObject);
                }
            }           
        }
    }
    void Hit(Bullet bullet)
    {
        hp -= bullet.damage;
       
        if(hp<0)
        {
            Kill();
        }
    }
    void Kill()
    {
        ExploseAnim();       

        if(ShipKillEvent != null)
        {
            ShipKillEvent(this, fleet);
        }

        Destroy(gameObject);
    }
    void ExploseAnim()
    {
        GameObject ex = Instantiate(exposion);
        ex.transform.position = transform.position;
    }

}
